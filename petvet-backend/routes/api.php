<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::fallback(function(){
    return response()->api( 'error', ['message' => 'Page Not Found. If error persists, contact jesusantacruz24@gmail.com'], 404);
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('sales/current_invoice','Sales\SalesController@currentInvoice');
Route::resource('clients', 'Clients\ClientsController');
Route::resource('employees', 'Employees\EmployeesController');
Route::resource('products', 'Products\ProductsController');
Route::resource('services', 'Services\ServicesController');
Route::resource('sales', 'Sales\SalesController');
Route::get('clients/{id}/bonuses','Clients\ClientsController@findBonuses');

