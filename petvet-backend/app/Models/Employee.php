<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
 protected $fillable = ['name','job','daily_salary','hiring_date','address','phone_number'];
}
