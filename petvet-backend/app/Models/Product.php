<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['price','category','max_stock','min_stock','description','location','name','brand'];
}
