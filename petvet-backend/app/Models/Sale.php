<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $primaryKey = 'invoice';

    //protected $appends = ['services','products','bonuses'];
    public $timestamps = false;

    public function bonuses() {
        return $this->hasMany(Bonus::class, 'invoice');
    }

    public function services() {
        return $this->hasMany(ServiceDetail::class, 'invoice');
    }

    public function products() {
        return $this->hasMany(SaleDetail::class, 'invoice');
    }

    public function employee() {
        return $this->hasOne(Employee::class,'id','employee_id');
    }

    public function client() {
        return $this->hasOne(Client::class,'phone_number','phone_number');
    }
}
