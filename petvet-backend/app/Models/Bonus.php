<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    protected $fillable = ['amount','invoice','phone_number'];

    public $timestamps = false;

    public function client() {
        return $this->belongsTo(Client::class,'phone_number');
    }

    public function sale() {
        return $this->belongsTo(Sale::class,'invoice');
    }
}
