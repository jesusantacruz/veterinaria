<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model
{
    protected $table = 'sales_detail';

    protected $fillable = ['invoice','product_id','quantity','price'];

    public $timestamps = false;

    protected $primaryKey = null;

    public $incrementing = false;

    public function sale(){
        return $this->belongsTo(Sale::class, 'invoice');
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
