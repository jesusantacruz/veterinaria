<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceDetail extends Model
{
    protected $table = 'services_detail';

    protected $fillable = ['invoice','service_id','quantity','price'];

    public $timestamps = false;

    protected $primaryKey = null;

    public $incrementing = false;

    public function sale(){
        return $this->belongsTo(Sale::class, 'invoice');
    }

    public function service(){
        return $this->belongsTo(Service::class);
    }
}
