<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use softDeletes;

    protected $primaryKey = 'phone_number';

    protected $keyType = 'string';

    protected $fillable = ['phone_number','name','address','rfc','email','bonus'];

    public function getPhoneNumberAttribute($attribute) {
        return (string)$attribute;
    }

    public function bonuses() {
        return $this->hasMany(Bonus::class, 'phone_number');
    }
}
