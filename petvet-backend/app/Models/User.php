<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;

    use SoftDeletes;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name', 'email', 'password', 'person_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function accessRecords()
    {
        return $this->hasMany(AccessRecord::class);
    }

    public function cardRegistrations()
    {
        return $this->hasMany(CardRegistration::class);
    }

    public function cards()
    {
        return $this->belongsToMany(CardRfid::class, 'card_registration', 'user_id', 'card_rfid_id')
            ->WithPivot(['assigned_at', 'unassigned_at']);
    }

    public function justificationTeachers()
    {
        return $this->hasMany(JustificationTeacher::class, 'user_id');
    }

    public function credentialApplication()
    {
        return $this->hasMany(CredentialApplication::class, 'user_id');
    }

}
