<?php

namespace App\Http\Controllers\Sales;

use App\Http\Controllers\Controller;
use App\Models\Bonus;
use App\Models\Product;
use App\Models\Sale;
use App\Models\Client;
use App\Models\SaleDetail;
use App\Models\ServiceDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sale = Sale::all();

        return response()->api('success',$sale);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sale = new Sale;
        $sale->date = Carbon::now();
        $sale->phone_number = $request->phone_number;
        $sale->employee_id = $request->employee_id;
        $bonusAcumulated = 0.0;
        $total = 0.0;
        $sale->save();

        foreach ($request->items as $item) {
            if($item['type'] == 'product') {
                $saleDetail = new SaleDetail([
                    'invoice' => $sale->invoice,
                    'product_id' => $item['code'],
                    'quantity' => $item['quantity'],
                    'price' => $item['price']
                ]);

                $product = Product::findOrFail($item['code']);
                $product->inventory = $product->inventory - $item['quantity'];
                $product->save();

                $saleDetail->saveOrFail();
            } else {
                $serviceDetail = new ServiceDetail([
                    'invoice' => $sale->invoice,
                    'service_id' => $item['code'],
                    'quantity' => $item['quantity'],
                    'price' => $item['price']
                ]);

                $serviceDetail->saveOrFail();
            }
            $total += ($item['quantity'] * $item['price']);
        }


        foreach ($request->bonuses as $bonus) {
            $b = Bonus::findOrFail($bonus['id']);
            $b->invoice = $sale->invoice;
            $bonusAcumulated += $b->amount;
            $b->save();
        }

        $total -= $bonusAcumulated;

        $client = Client::findOrFail($request->phone_number);
        $clientBonus = $client->bonus + $total;
        $nBonus = (int)($clientBonus/1000);
        if($nBonus > 0) {
            for($i = 0; $i < $nBonus; $i++) {
                $createBonus = new Bonus([
                    'phone_number' => $request->phone_number,
                    'amount' => 50.0
                ]);

                $createBonus->saveOrFail();
            }
        }

        $client->bonus = $clientBonus - ($nBonus * 1000);
        $client->save();



        return response()->api('success',$sale);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sale = Sale::where('invoice','=',$id)->with(['products.product','services.service','bonuses','client','employee'])->get();
        if(count($sale) > 0) {
            return response()->api('success',$sale[0]);
        } else {
            return response()->api('error','No existe la venta');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sale = Sale::findOrFail($id);
        $sale->delete();

        return response()->api('success',$sale);
    }

    public function currentInvoice() {
        $sale = Sale::select('invoice')->orderBy('invoice','DESC')->limit(1)->get();
        if(count($sale) < 1){
            return response()->api('success',['invoice'=> 1]);
        }
        $sale[0]->invoice += 1;
        return response()->api('success',$sale[0]);
    }
}
