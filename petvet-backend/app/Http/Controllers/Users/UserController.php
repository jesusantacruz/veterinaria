<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function store(Request $request)
    {
        $user = User::create($request->all());
        $user ->save();

        return response()->api('success', $user);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        //$cards = $user->cardRegistration()->active()->get();

        //return $user;

        return response()->api('success', $user);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->email = $request->email;
        $user->password  = $request->password;
        $user->user_name = $request->user_name;
        $user->person_id = $request->person_id;

        $user ->save();
        return response()->api('success',$user);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return response()->api('success',$user);
    }

    public function showCardsActive($id)
    {
        $user = User::findOrFail($id);

        return response()->api('success',$user);
    }
}
