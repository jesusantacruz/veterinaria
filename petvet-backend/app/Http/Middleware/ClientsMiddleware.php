<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

class ClientsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $result = Validator::make($request->all(), [
            'phone_number' => 'required',
            'name'=> 'required',
            'address'=> 'required',
            'rfc'=> 'required',
            'email'=> 'required|email',
            'bonus'=> 'required'
        ]);

        if($result->fails())
        {
            $error_text = "";
            $errors = $result->errors();
            foreach ($errors->all() as $message){
                $error_text.= $message.' ';
            }
            return response()->api('error',$error_text);
        }

        return $next($request);
    }
}
