<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

class ProductsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $result = Validator::make($request->all(), [
            'price' => 'required',
            'category' => 'required',
            'max_stock' => 'required|numeric',
            'min_stock' => 'required|numeric',
            'description' => 'required',
            'location'=> 'required',
            'name' => 'required',
            'brand' => 'required'
        ]);

        if($result->fails())
        {
            $error_text = "";
            $errors = $result->errors();
            foreach ($errors->all() as $message){
                $error_text.= $message.' ';
            }
            return response()->api('error',$error_text);
        }

        return $next($request);

    }
}
