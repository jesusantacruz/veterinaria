<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

class EmployeesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $result = Validator::make($request->all(), [
            'name' => 'required',
            'job' => 'required|in:cashier,veterinarian,storer,receptionist,administration,manager,cleaning',
            'daily_salary' => 'required',
            'hiring_date' => 'required|date',
            'address' => 'required',
            'phone_number' => 'required|numeric'
        ]);

        if($result->fails())
        {
            $error_text = "";
            $errors = $result->errors();
            foreach ($errors->all() as $message){
                $error_text.= $message.' ';
            }
            return response()->api('error',$error_text);
        }

        return $next($request);

    }
}
