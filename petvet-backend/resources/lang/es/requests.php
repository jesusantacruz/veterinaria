<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Requests Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'type' => [
        'Teacher' => 'Profesor',
        'Employee.php'=> 'Empleado',
        'Student' => 'Alumno'
    ],
    'status' => [
        'Payment Adue' => 'Pago pendiente',
        'In Process' => 'En proceso',
        'Print' => 'En impresión',
        'Available' => 'Disponible',
        'Delivered' => 'Entregada'
    ]

];
