<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_detail', function (Blueprint $table) {
            $table->bigInteger('invoice');
            $table->bigInteger('service_id');
            $table->integer('quantity');
            $table->float('price');
            $table->foreign('invoice')->references('invoice')->on('sales');
            $table->foreign('service_id')->references('id')->on('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_detail');
    }
}
