import {iconMarkup} from 'sweetalert/typings/modules/markup';

const Clients = {
    text : 'Clientes',
    link : '/clients',
    icon : 'icon-people',
    submenu: [
        {
            text: 'Registrar',
            //link: '/clients/create'
          link : '/clients',
        },
        {
            text: 'Catalogo',
            //link: '/clients/all'
          link : '/clients',
        }]
}

const Products = {
    text : 'Productos',
    link : '/products',
    icon : 'icon-handbag',
    submenu: [
        {
            text: 'Registrar',
          link : '/clients',
//            link: '/products/create'
        },
        {
            text: 'Catalogo',
            //link: '/products/all'
          link : '/clients',
        }]
}

const Employees = {
    text : 'Empleados',
    link : '/employees',
    icon : 'icon-user',
    submenu: [
        {
            text: 'Registrar',
            //link: '/employees/create'
          link : '/clients',
        },
        {
            text: 'Catalogo',
          //link: '/employees/all'
          link : '/clients',
        }]
}

const Cashier = {
    text : 'Caja',
    //link : '/sales/cash',
  link : '/clients',
    icon : 'icon-wallet',
}

const Sales = {
    text : 'Ventas',
    link : '/sales',
    icon : 'icon-folder',
    submenu: [
        {
            text: 'Consulta',
            //link: '/sales/find'
          link : '/clients',
        }]
}



export const menu = [
    Clients,
    Products,
    Employees,
    Cashier,
    Sales
];

