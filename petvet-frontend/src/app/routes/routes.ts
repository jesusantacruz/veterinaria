import { LayoutComponent } from '../layout/layout.component';

export const routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'clients', pathMatch: 'full' },
            { path: 'clients', loadChildren: () => import('./components/clients/clients.module').then(m => m.ClientsModule) },
            { path: 'products', loadChildren: () => import('./components/products/products.module').then(m => m.ProductsModule) },
            { path: 'employees', loadChildren: () => import('./components/employees/employees.module').then(m => m.EmployeesModule) },
            { path: 'sales', loadChildren: () => import('./components/sales/sales.module').then(m => m.SalesModule) }
        ]
    },


    { path: '**', redirectTo: 'clients' }

];
