import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {HostService} from './host.service';
import {Observable} from 'rxjs';

@Injectable()
export class EmployeesService {

  constructor(private _http: HttpClient,
              private _host: HostService) {
  }

  public create(object): Observable<any> {
    return this._http.post(this._host.getName() + '/api/employees', object);
  }

  public all(): Observable<any> {
    return this._http.get(this._host.getName() + '/api/employees');
  }

  public update(object, id): Observable<any> {
    return this._http.patch(this._host.getName() + '/api/employees/' + id, object);
  }

  public find(phoneNumber): Observable<any> {
    return this._http.get(this._host.getName() + '/api/employees/' + phoneNumber);
  }
}

