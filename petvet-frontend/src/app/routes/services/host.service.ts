import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HostService {
  private readonly name: string;
  constructor() {
    this.name = 'http://127.0.0.1:8000';
  }
  public getName(): string {
    return this.name;
  }
}
