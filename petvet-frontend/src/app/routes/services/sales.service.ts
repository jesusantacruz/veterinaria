import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {HostService} from './host.service';
import {Observable} from 'rxjs';

@Injectable()
export class SalesService {

  constructor(private _http: HttpClient,
              private _host: HostService) {
  }

  public all(): Observable<any> {
    return this._http.get(this._host.getName() + '/api/sales');
  }

  public store(object): Observable<any> {
    return this._http.post(this._host.getName() + '/api/sales', object);
  }

  public find(invoice: string): Observable<any> {
    return this._http.get(this._host.getName() + '/api/sales/' + invoice);
  }

  public invoice(): Observable<any> {
    return this._http.get(this._host.getName() + '/api/sales/current_invoice');
  }
}

