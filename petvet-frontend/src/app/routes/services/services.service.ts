import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {HostService} from './host.service';
import {Observable} from 'rxjs';

@Injectable()
export class ServicesService {

  constructor(private _http: HttpClient,
              private _host: HostService) {
  }

  public all(): Observable<any> {
    return this._http.get(this._host.getName() + '/api/services');
  }
}

