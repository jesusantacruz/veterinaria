import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductsService } from '../../../services/products.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HostService} from '../../../services/host.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import * as moment from 'moment';

const swal = require('sweetalert');

@Component({
     selector: 'app-edit-employee',
     templateUrl: './edit.component.html',
     styleUrls: ['./edit.component.css'],
     providers: [ProductsService, HostService, ToasterService]
})
export class EditComponent implements OnInit {
    public id;
     public product = {price: '', category: '', min_stock: '', inventory: '', max_stock: '', description: '', location: '', name : '', brand : ''};
     public productRegistration: FormGroup;
     public fecha;
     private toaster: any;
     private toasterConfig: any;
     private toasterconfig: ToasterConfig = new ToasterConfig({
          positionClass: 'toast-bottom-right',
          showCloseButton: false
     });
     constructor(private _httpProducts: ProductsService,
                    private _route: ActivatedRoute,
                    private _host: HostService,
                    private toasterService: ToasterService,
                    private router: Router,
                    private fb: FormBuilder) {
          this.productRegistration = fb.group({
               price        : [this.product.price, Validators.compose([
                    Validators.required,
                    Validators.pattern('^[0-9.]+$')])],
               category     : [this.product.category, Validators.required],
               min_stock    : [this.product.min_stock, Validators.compose([
                    Validators.required,
                    Validators.pattern('^[0-9]+$')])],
               max_stock    : [this.product.max_stock, Validators.compose([
                    Validators.required,
                    Validators.pattern('^[0-9]+$')])],
                inventory    : [this.product.inventory, Validators.compose([
                  Validators.required,
                  Validators.pattern('^[0-9]+$')])],
               description  : [this.product.description, Validators.required],
               location     : [this.product.location, Validators.required],
               name         : [this.product.name, Validators.required],
               brand        : [this.product.brand, Validators.required]
          });
           this._route.params.forEach( (params: Params) => { this.id = params.id; } );
     }
     public ngOnInit() {
               this._httpProducts.find(this.id).subscribe((res: any) => {
                if (res.status === 'success') {
                     this.product = res.data;
                } else {
                     this.popError(res.data);
                }
           });
      }

     public save($ev) {
          $ev.preventDefault();
          for(let c in this.productRegistration.controls) {
               this.productRegistration.controls[c].markAsTouched();
          }
          if (this.productRegistration.valid) {
               console.log('Valid!');
               this._httpProducts.update(this.productRegistration.value, this.id).subscribe( (res: any) => {
                    if (res.status === 'success') {
                         this.sweetalertDemo3();
                    } else {
                         this.popError(res.data);
                    }
               });
          }

     }

     public popError(error) {
          this.toasterService.pop('error', 'Error', error);
     }

     public sweetalertDemo3() {
          swal('Registro exitoso!', 'click en ok para continuar', 'success')
               .then((isConfirm) => {
                    if (isConfirm) {
                         this.router.navigate(['/products/all']);
                    }
               });
     }

}
