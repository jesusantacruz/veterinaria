import { NgModule } from '@angular/core';
import { RouterModule, Routes, RouterLinkActive } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';

import { CreateComponent } from './create/create.component';
import { FileUploadModule } from 'ng2-file-upload';
import { ImageCropperModule } from 'ng2-img-cropper';
import {EditComponent} from './edit/edit.component';
import {ListComponent} from './list/list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { Ng2TableModule } from 'ng2-table/ng2-table';

const routes: Routes =  [
	{ path: 'create' , component: CreateComponent },
	{ path: 'edit/:id' , component: EditComponent},
	{ path: 'all', component: ListComponent}];


@NgModule({

	imports:[RouterModule.forChild(routes), FormsModule, SharedModule, FileUploadModule, ImageCropperModule, NgSelectModule, Ng2TableModule],
	exports:[RouterModule],
	declarations:[CreateComponent, EditComponent, ListComponent]

	})
export class EmployeesModule {}