import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeesService } from '../../../services/employees.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HostService} from '../../../services/host.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import * as moment from 'moment';

const swal = require('sweetalert');

@Component({
    selector: 'app-create-employee',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css'],
    providers: [EmployeesService, HostService, ToasterService]
})
export class CreateComponent implements OnInit {
    public employee = {name: '', job: '', daily_salary: '', hiring_date: '', address: '', phone_number: ''};
    public employeeRegistration : FormGroup;
    public fecha;
    private toaster: any;
    private toasterConfig: any;
    private toasterconfig: ToasterConfig = new ToasterConfig({
        positionClass: 'toast-bottom-right',
        showCloseButton: false
    });
    constructor(private _httpEmployees: EmployeesService,
                private _route: ActivatedRoute,
                private _host: HostService,
                private toasterService: ToasterService,
                private router: Router,
                private fb: FormBuilder) {
        this.employeeRegistration = fb.group({
            name            : [this.employee.name, Validators.required],
            job				: [this.employee.job, Validators.required],
            daily_salary    : [this.employee.daily_salary, Validators.compose([
                Validators.required,
                Validators.pattern('^[0-9.]+$')])],
            hiring_date     : [this.employee.hiring_date, Validators.required],
            address         : [this.employee.address, Validators.required],
            phone_number    : [this.employee.phone_number, Validators.compose([
                Validators.required,
                Validators.pattern('^[0-9]+$'),
                CustomValidators.rangeLength([10, 10])])]
        });
    }
    public ngOnInit() {
        this.fecha =  moment().add('days', 365).format('YYYY-MM-DD');
    }


    public save($ev) {
        $ev.preventDefault();
        for(let c in this.employeeRegistration.controls) {
            this.employeeRegistration.controls[c].markAsTouched();
        }
        if (this.employeeRegistration.valid) {
            console.log('Valid!');
            this._httpEmployees.create(this.employeeRegistration.value).subscribe( (res: any) => {
                if (res.status === 'success') {
                    this.sweetalertDemo3();
                } else {
                    this.popError(res.data);
                }
            });
        }

    }

    public popError(error) {
        this.toasterService.pop('error', 'Error', error);
    }

    public sweetalertDemo3() {
        swal('Registro exitoso!', 'click en ok para continuar', 'success')
            .then((isConfirm) => {
                if (isConfirm) {
                    this.router.navigate(['/employees/all']);
                }
            });
    }

}
