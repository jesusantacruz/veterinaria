import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientsService } from '../../../services/clients.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HostService} from '../../../services/host.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {ToasterConfig, ToasterService} from 'angular2-toaster';

const swal = require('sweetalert');

@Component({
    selector: 'app-create-client',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css'],
    providers: [ClientsService, HostService, ToasterService]
})
export class CreateComponent implements OnInit {
    public client = {phone_number: '', name: '', address: '', rfc: '', email: '', bonus: 0.0};
    public clientRegistration: FormGroup;
    private toaster: any;
    private	toasterConfig: any;
    private	toasterconfig: ToasterConfig = new ToasterConfig({
        positionClass: 'toast-bottom-right',
        showCloseButton: false
    });
    constructor(private _httpClients: ClientsService,
                private _route: ActivatedRoute,
                private _host: HostService,
                private toasterService: ToasterService,
                private router: Router,
                private fb: FormBuilder) {
        this.clientRegistration = fb.group({
            phone_number	: [this.client.phone_number, Validators.compose([
                Validators.required,
                Validators.pattern('^[0-9]+$'),
                CustomValidators.rangeLength([10, 10])])],
            name    		: [this.client.name, Validators.required],
            address 		: [this.client.address, Validators.required],
            rfc				: [this.client.rfc, Validators.required],
            email 			: [this.client.email, Validators.compose([Validators.required, CustomValidators.email])],
            bonus			: [this.client.bonus]
        });
    }
public ngOnInit() {}


public save($ev) {
    $ev.preventDefault();
    for(let c in this.clientRegistration.controls) {
        this.clientRegistration.controls[c].markAsTouched();
    }
    if (this.clientRegistration.valid) {
        console.log('Valid!');
        this._httpClients.create(this.clientRegistration.value).subscribe( (res: any) => {
            if (res.status === 'success') {
                this.sweetalertDemo3();
            } else {
                this.popError(res.data);
            }
        });
    }
    console.log(this.clientRegistration.value);
}

public popError(error) {
    this.toasterService.pop('error', 'Error', error);
}

public sweetalertDemo3() {
    swal('Registro exitoso!', 'click en ok para continuar', 'success')
        .then((isConfirm) => {
            if (isConfirm) {
                this.router.navigate(['/clients/all']);
            }
        });
}

}
