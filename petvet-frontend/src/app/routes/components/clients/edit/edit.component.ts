import { Component, OnInit, ViewChild } from '@angular/core';
import { ImageCropperComponent, CropperSettings, Bounds } from 'ng2-img-cropper';
import { ClientsService } from '../../../services/clients.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HostService} from '../../../services/host.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {ToasterConfig, ToasterService} from 'angular2-toaster';

const swal = require('sweetalert');

@Component({
    selector: 'app-edit-client',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
    providers: [ClientsService, HostService, ToasterService]
    })
export class EditComponent implements OnInit {
public phoneNumber: string;
public client = {phone_number: '', name: '', address: '', rfc: '', email: '', bonus: 0.0};
public clientRegistration: FormGroup;
private toaster: any;
private	toasterConfig: any;
private	toasterconfig: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-right',
    showCloseButton: false
});
constructor(
    private _httpClient: ClientsService,
    private _route: ActivatedRoute,
    private _host: HostService,
    private router: Router,
    private toasterService: ToasterService,
    private fb: FormBuilder) {
    this.clientRegistration = fb.group({
        phone_number	:  new FormControl({value : this.client.phone_number, disabled: true}),
        name    		: [this.client.name, Validators.required],
        address 		: [this.client.address, Validators.required],
        rfc				: [this.client.rfc, Validators.required],
        email 			: [this.client.email, Validators.compose([Validators.required, CustomValidators.email])],
        bonus			: [this.client.bonus]
    });
    this._route.params.forEach( (params: Params) => { this.phoneNumber = params.phone; } );
}

public ngOnInit() {
    this._httpClient.find(this.phoneNumber).subscribe((res: any) => {
        if (res.status === 'success') {
            this.client = res.data;
        } else {
            this.popError(res.data);
        }
    });
}
public popError(error) {
    this.toasterService.pop('error', 'Error', error);
}

public save($ev) {
    $ev.preventDefault();
    for(let c in this.clientRegistration.controls) {
        this.clientRegistration.controls[c].markAsTouched();
    }
    if (this.clientRegistration.valid) {
        console.log('Valid!');
        this._httpClient.update(this.clientRegistration.value, this.phoneNumber).subscribe( (res: any) => {
            if (res.status === 'success') {
                this.sweetalertDemo3();
            } else {
                this.popError(res.data);
            }
        });
    }
	console.log(this.clientRegistration.value);
}

public sweetalertDemo3() {
    swal('Registro exitoso!', 'click en ok para continuar', 'success')
        .then((isConfirm) => {
            if (isConfirm) {
                this.router.navigate(['/clients/all']);
            }
        });
}
}
