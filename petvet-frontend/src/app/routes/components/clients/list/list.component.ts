import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientsService } from '../../../services/clients.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HostService} from '../../../services/host.service';
import {ToasterConfig, ToasterService} from 'angular2-toaster';

@Component({
    selector: 'app-list-client',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css'],
    providers: [ClientsService, HostService, ToasterService]
    })

export class ListComponent implements OnInit {
public ng2TableData: Array<any> = [];
private toaster: any;
private	toasterConfig: any;
private	toasterconfig: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-right',
    showCloseButton: false
});
constructor(
    private _httpClient: ClientsService,
    private _route: ActivatedRoute,
    private _host: HostService,
    private router: Router,
    private toasterService: ToasterService) {
    this.length = this.ng2TableData.length;
}
public rows: Array<any> = [];
public columns: Array<any> = [
    { title: 'Núimero telefónico', name: 'phone_number', filtering: { filterString: '', placeholder: 'Filtrar por teléfono' }} ,
    { title: 'Nombre', name: 'name', filtering: { filterString: '', placeholder: 'Filtrar por nombre' }},
    { title: 'Dirección', name: 'address', filtering: { filterString: '', placeholder: 'Filtrar por direccion' } },
    { title: 'RFC', name: 'rfc', filtering: { filterString: '', placeholder: 'Filtrar por RFC' } },
    { title: 'Correo electrónico', name: 'email', filtering: { filterString: '', placeholder: 'Filtrar por email' } },
    { title: 'Bonificación', name: 'bonus' }
    ];
public page: number = 1;
public itemsPerPage: number = 10;
public maxSize: number = 5;
public numPages: number = 1;
public length: number = 0;

public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered', 'mb-0', 'd-table-fixed']
    };


public ngOnInit() {
    this._httpClient.all().subscribe( (res: any) => {
        if (res.status === 'success') {
            this.ng2TableData = res.data;
            this.onChangeTable(this.config);
        } else {
            this.popError(res.data);
        }
    });
}

public popSuccess() {
    this.toasterService.pop('success', 'OK', 'Operación exitosa');
}

public popError(error) {
    this.toasterService.pop('error', 'Error', error);
}

public changePage(page: any, data: Array<any> = this.ng2TableData): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
}

public changeSort(data: any, config: any): any {
    if (!config.sorting) {
        return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
         if (columns[i].sort !== '' && columns[i].sort !== false) {
             columnName = columns[i].name;
             sort = columns[i].sort;
         }
    }

    if (!columnName) {
         return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
        if (previous[columnName] > current[columnName]) {
            return sort === 'desc' ? -1 : 1;
        } else if (previous[columnName] < current[columnName]) {
            return sort === 'asc' ? -1 : 1;
         }
        return 0;
    });
}

public changeFilter(data: any, config: any): any {

    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
        if (column.filtering) {
            filteredData = filteredData.filter((item: any) => {
            return item[column.name].match(column.filtering.filterString);
        });
        }
    });

    if (!config.filtering) {
        return filteredData;
    }

    if (config.filtering.columnName) {
        return filteredData.filter((item: any) =>
            item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
        let flag = false;
        this.columns.forEach((column: any) => {
            if (item[column.name].toString().match(this.config.filtering.filterString)) {
                flag = true;
            }
        });
        if (flag) {
            tempArray.push(item);
        }
    });
    filteredData = tempArray;

    return filteredData;
}

public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
        (<any>Object).assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
        (<any>Object).assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.ng2TableData, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
}

public onCellClick(data: any): any {
    console.log(data);
    this.router.navigate(['/clients/edit/' + data.row.phone_number]);
}
}
