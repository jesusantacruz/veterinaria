import { Component, OnInit } from '@angular/core';
import {ClientsService} from '../../../services/clients.service';
import { FormControl } from '@angular/forms';
import {ProductsService} from '../../../services/products.service';
import {SalesService} from '../../../services/sales.service';
//import {ServicesService} from '../../../services/services.service';
import {HostService} from '../../../services/host.service';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ServicesService} from '../../../services/services.service';
import {map, startWith} from 'rxjs/operators';
import {Router} from '@angular/router';

const swal = require('sweetalert');

@Component({
  selector: 'app-cash',
  templateUrl: './cash.component.html',
  styleUrls: ['./cash.component.scss'],
  providers: [ClientsService, HostService, ToasterService, ProductsService, ServicesService, SalesService]
})


export class CashComponent implements OnInit {
  private invoice: number;
  private bonus: boolean;
  private productCtl: FormControl;
  private numberProductsCtl: FormControl;
  private filteredProducts: any;
  private products: Array<any>;

  private serviceCtl: FormControl;
  private numberServicesCtl: FormControl;
  private filteredServices: any;
  private services: Array<any>;

  private items: Array<any>;
  private bonuses: Array<any>;

  private client = {address: '', bonus: 0.0, email: '', name: '', phone_number: '', rfc: ''};
  public phone: string;
  private sale = {
    phone_number: '',
    employee_id: '',
    items: [],
    bonuses: []
  };

  private subtotal: number;
  private tax: number;
  private total: number;

  private clientFound = false;
  private productValid = false;
  private serviceValid = false;
  private toaster: any;
  private	toasterConfig: any;
  private	toasterconfig: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-right',
    showCloseButton: true
  });

  constructor(private _httpClient: ClientsService,
              private _httpProduct: ProductsService,
              private _httpService: ServicesService,
              private _httpSales: SalesService,
              private toasterService: ToasterService,
              private router: Router) {

    this.productCtl = new FormControl();
    this.serviceCtl = new FormControl();
    this.numberProductsCtl = new FormControl([1]);
    this.numberServicesCtl = new FormControl([1]);
    this.filteredServices = this.serviceCtl.valueChanges
      .pipe(startWith(null))
      .pipe(map(name => this.filterServices(name)));

    this.filteredProducts = this.productCtl.valueChanges
      .pipe(startWith(null))
      .pipe(map(name => this.filterProducts(name)));
    this.items = [];
    this.subtotal = 0.0;
    this.bonus = false;
    this.invoice = 0;
  }

  filterProducts(val: string) {
    //return val ? this.products.filter((s) => new RegExp(val, 'gi').test(s.name)) : this.products;
    return val ? this.products.filter((s) => new RegExp(val, 'gi').test(this.getName(s))) : this.products;
  }

  filterServices(val: string) {
    return val ? this.services.filter((s) => new RegExp(val, 'gi').test(this.getDescription(s))) : this.services;
  }

  public getName(value: any) {
    let find: string;
    if (value && value.name) {
      find = value.id + ' - ' + value.name;
      return find;
    }
    return '';
    //return value && value.name ? value.name : '';
  }

  public getDescription(value: any) {
    let find: string;
    if (value && value.description) {
      find = value.id + ' - ' + value.description;
      return find;
    }
    return '';
    //return value && value.description ? value.description : '';
  }

  public clearProduct(event) {
    console.log(event);
    if (event.keyCode != 13) {
      this.productValid = false;
    }
  }

  public viewProduct() {
    this.productValid = true;
  }

  public clearService(event) {
    console.log(event);
    if (event.keyCode != 13) {
      this.serviceValid = false;
    }
  }

  public viewService() {
    this.serviceValid = true;
  }

  ngOnInit() {
    this.getServices();
    this.getProducts();
    this.getInvoice();
  }

  public getInvoice() {
    this._httpSales.invoice().subscribe((res: any) => {
      this.invoice = res.data.invoice;
    });
  }

  public getServices() {
    this._httpService.all().subscribe((res: any) => {
      this.services = res.data;
    });
  }

  public getProducts() {
    this._httpProduct.all().subscribe((res: any) => {
      this.products = res.data;
    });
  }

  public popError(error: string) {
    this.toasterService.pop('error', 'Error', error);
  }

  public popSuccess(message: string) {
    this.toasterService.pop('success', 'Exito', message);
  }

  public findClient() {
    this._httpClient.find(this.phone).subscribe((res: any) => {
      if (res.status === 'success') {
        this.client = res.data;
        this.popSuccess('Cliente encontrado ' + this.client.phone_number);
        this.clientFound = true;
        this.findBonuses();
        this.getProducts();
      } else {
        this.popError('Cliente no encontrado');
        swal({
          title: 'Cliente no encontrado, desea ir a registrar?',
          text: name,
          icon: 'warning',
          buttons: {
            cancel: {
              text: 'No!',
              value: null,
              visible: true,
              className: "",
              closeModal: true
            },
            confirm: {
              text: 'Si!',
              value: true,
              visible: true,
              className: "bg-success",
              closeModal: true
            }
          }
        }).then((isConfirm) => {
          if (isConfirm) {
            this.router.navigate(['/clients/create']);
          } else {
            swal('Cancelado!', 'No se registrado el cliente', 'error');
          }
        });

        this.clientFound = false;
      }});
  }

  public findBonuses() {
    this._httpClient.findBonuses(this.client.phone_number).subscribe((res: any) => {
      if (res.status === 'success') {
        this.bonuses = res.data;
        this.bonuses = this.bonuses.filter((item: any) => {
          return (item.invoice == null);
        });
      } else {
        this.bonuses = [];
      }});
  }

  public addProduct($event) {
    console.log(this.productCtl);
    if (!this.numberProductsCtl.value || this.numberProductsCtl.value < 1) {
      this.popError('Necesitas especificar una cantidad valida!');
      return;
    }
    let numberOfItems: number;
    let sub: number;
    const item = this.findItem('product', this.productCtl.value.id);
    if (item >= 0) {
      let total: number;
      // tslint:disable-next-line:radix
      total = parseInt(this.items[item].quantity) + parseInt(this.numberProductsCtl.value);
      if (total > this.productCtl.value.inventory) {
        console.log(total);
        this.popError('No hay suficientes productos en el inventario');
      } else {
        this.popSuccess('Producto(s) añadido(s)');
        this.items[item].quantity = total;
        this.productValid = false;
        this.productCtl.reset();
        this.numberProductsCtl.reset(1);
        this.calculateSubtotal();
      }
      return;
    }

    sub = this.productCtl.value.price * this.numberProductsCtl.value;
    numberOfItems = this.items.length < 1 ? 1 : this.items.length;
    if (this.productCtl.value.inventory < this.numberProductsCtl.value) {
      this.popError('No hay suficientes productos en el inventario');
    } else {
      this.items.push({
        type: 'product',
        number: numberOfItems,
        code: this.productCtl.value.id,
        name: this.productCtl.value.name,
        description: this.productCtl.value.description,
        price: this.productCtl.value.price,
        quantity: parseInt(this.numberProductsCtl.value),
        subTotal: sub
      });
      this.productValid = false;
      this.productCtl.reset();
      this.numberProductsCtl.reset(1);
      this.calculateSubtotal();
      this.popSuccess('Producto(s) añadido(s)');
    }
  }

  public addService($event) {
    console.log(this.serviceCtl);
    if (!this.numberServicesCtl.value || this.numberServicesCtl.value < 1) {
      this.popError('Necesitas especificar una cantidad valida!');
      return;
    }
    let numberOfItems: number;
    let sub: number;
    const item = this.findItem('service', this.serviceCtl.value.id);
    if (item >= 0) {
      let total: number;
      // tslint:disable-next-line:radix
      total = parseInt(this.items[item].quantity) + parseInt(this.numberServicesCtl.value);
      this.popSuccess('Añadido');
      this.items[item].quantity = total;
      this.serviceValid = false;
      this.serviceCtl.reset(1);
      this.numberServicesCtl.reset();
      return;
    }
    sub = this.serviceCtl.value.price * this.numberServicesCtl.value;
    numberOfItems = this.items.length < 1 ? 1 : this.items.length;
    this.items.push({
      type: 'service',
      number: numberOfItems,
      code: this.serviceCtl.value.id,
      name: 'Servicio',
      description: this.serviceCtl.value.description,
      price: this.serviceCtl.value.price,
      quantity: parseInt(this.numberServicesCtl.value),
      subTotal: sub
    });
    this.serviceValid = false;
    this.serviceCtl.reset(1);
    this.numberServicesCtl.reset();
    this.popSuccess('Servicio(s) añadido(s)');
    this.calculateSubtotal();
  }


  public calculateSubtotal() {
    this.subtotal = 0.0;
    let nItems: number;
    nItems = this.items.length;

    for ( let i = 0; i < nItems; i++) {
      this.subtotal += this.items[i].subTotal;
    }
    if (this.bonus == true) {
      this.subtotal -= this.calculateBonus();
      this.subtotal = this.subtotal < 0 ? 0 : this.subtotal;
    }
    this.tax = this.subtotal * 0.16;
    this.total = this.tax + this.subtotal;
  }

  private calculateBonus(): number {
    let bonusAmount: number;
    bonusAmount = 0;
    this.sale.bonuses.splice(0,this.sale.bonuses.length);
    for (let i = 0; i < this.bonuses.length; i++) {
      if (this.bonuses[i].apply) {
        // tslint:disable-next-line:radix
        bonusAmount += parseInt(this.bonuses[i].amount);
        this.sale.bonuses.push(this.bonuses[i]);
      }
    }
    return bonusAmount;
  }

  public toggleBonus() {
    this.calculateSubtotal();
  }

  public applyBonus() {
    this.calculateSubtotal();
  }

  public removeItem(index: number) {
    const name = this.items[index].name;
    swal({
        title: 'Realmente desea eliminar?',
        text: name,
        icon: 'warning',
        buttons: {
          cancel: {
            text: 'No!',
            value: null,
            visible: true,
            className: "",
            closeModal: true
          },
          confirm: {
            text: 'Si, eliminar!',
            value: true,
            visible: true,
            className: "bg-danger",
            closeModal: false
          }
        }
      }).then((isConfirm) => {
        if (isConfirm) {
          this.items.splice(index, 1);
          this.calculateSubtotal();
          swal('Eliminado!', 'El producto se removio correctamente.', 'success');
        } else {
         // swal('Cancelado!', 'No se ha eliminado el producto', 'error');
        }
      });
  }

  public resetCash() {
    this.clientFound = false;
    this.items.splice(0, this.items.length);
    this.productCtl.reset();
    this.serviceCtl.reset();
    this.numberProductsCtl.reset(1);
    this.numberServicesCtl.reset(1);
    this.serviceValid = false;
    this.productValid = false;
    this.phone = '';
    this.getInvoice();
  }

  private findItem(type: string, id: number) {
      for (let i = 0; i < this.items.length; i++) {
        if (this.items[i].code == id && this.items[i].type == type) {
          return i;
        }
      }
      return -1;
  }

  private saveSale() {
    this.sale.employee_id = '2';
    this.sale.phone_number = this.client.phone_number;
    this.sale.items = this.items;

    this._httpSales.store(this.sale).subscribe((res: any) => {
      if (res.status === 'success') {
        this.popSuccess('Venta registrada');
        this.getProducts();
        this.resetCash();
      } else {
        this.popError('Error');
      }
    });
    console.log(this.sale);
  }
}



