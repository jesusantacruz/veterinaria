import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, RouterLinkActive } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { CashComponent } from './cash/cash.component';
import { FindComponent } from './find/find.component';

const routes: Routes =  [
  { path: 'cash' , component: CashComponent },
  { path: 'find' , component: FindComponent }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), FormsModule, SharedModule ],
  exports: [RouterModule],
  declarations: [CashComponent, FindComponent]
})
export class SalesModule { }
