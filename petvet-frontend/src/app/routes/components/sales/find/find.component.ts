import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SalesService} from '../../../services/sales.service';
import {ClientsService} from '../../../services/clients.service';
import {HostService} from '../../../services/host.service';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ProductsService} from '../../../services/products.service';
import {ServicesService} from '../../../services/services.service';


@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.scss'],
  providers: [ClientsService, HostService, ToasterService, ProductsService, ServicesService, SalesService]
})
export class FindComponent implements OnInit {
  private invoice: string;
  private sale: any;
  private saleFound: boolean;
  private subtotal: number;
  private tax: number;
  private bonus: number;
  private total: number;
  private toaster: any;
  private	toasterConfig: any;
  private	toasterconfig: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-right',
    showCloseButton: true
  });

  constructor(private _httpSales: SalesService,
              private toasterService: ToasterService) {
    this.invoice = '';
    this.saleFound = false;
    this.subtotal = 0;
    this.tax = 0;
    this.bonus = 0;
    this.total = 0;
  }

  ngOnInit() {
  }


  private popError(error: string) {
    this.toasterService.pop('error', 'Error', error);
  }

  private popSuccess(message: string) {
    this.toasterService.pop('success', 'Correcto', message);
  }

  private findSale() {
    this._httpSales.find(this.invoice).subscribe((res: any) => {
      if (res.status == 'success') {
        this.sale = res.data;
        this.popSuccess('Venta encontrada');
        this.saleFound = true;
        this.generateTotals();
        console.log(this.sale);
      } else {
          this.saleFound = false;
          this.popError('No se encontro la venta con el folio: ' + this.invoice);
      }
    });
  }

  private generateTotals() {
    for (let i = 0; i < this.sale.products.length; i++) {
      this.subtotal += parseInt(this.sale.products[i].price ) * parseInt(this.sale.products[i].quantity);
    }

    for (let i = 0; i < this.sale.services.length; i++) {
      this.subtotal += parseInt(this.sale.services[i].price ) * parseInt(this.sale.services[i].quantity);
    }

    for (let i = 0; i < this.sale.bonuses.length; i++) {
      this.bonus += parseInt(this.sale.bonuses[i].amount);
    }

    this.subtotal -= this.bonus;
    this.subtotal = this.subtotal < 0 ? 0 : this.subtotal;
    this.tax = (this.subtotal * 0.16);
    this.total = this.subtotal + this.tax;

  }
}
