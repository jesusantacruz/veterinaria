import { Injectable } from '@angular/core';

@Injectable()
export class UserblockService {
    public userBlockVisible: boolean;
    constructor() {
        // initially visible
        //this.userBlockVisible  = true;
      /*
        Se deshabilita mientras se configuran los usuarios
       */
      this.userBlockVisible  = false;
    }

    getVisibility() {
        return this.userBlockVisible;
    }

    setVisibility(stat = true) {
        this.userBlockVisible = stat;
    }

    toggleVisibility() {
        this.userBlockVisible = !this.userBlockVisible;
    }

}
